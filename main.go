package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"
	"unicode/utf8"
)

const (
	timestampFormat = "1/2/06 3:04:05 PM"
)

var mountainLoc, pacificLoc *time.Location

func main() {
	var err error
	mountainLoc, err = time.LoadLocation("US/Mountain")
	if err != nil {
		log.Fatal(err)
	}

	pacificLoc, err = time.LoadLocation("US/Pacific")
	if err != nil {
		log.Fatal(err)
	}

	r := csv.NewReader(os.Stdin)

	headers, err := r.Read()
	if err != nil {
		log.Fatal(err)
	}

	w := csv.NewWriter(os.Stdout)
	w.Write(headers)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		var (
			fooDuration float64
			barDuration float64
			newRecord   = make([]string, len(record))
		)

		for i, entry := range record {
			switch headers[i] {
			case "FooDuration":
				fooDuration = entryToFloatingPointSeconds(entry)
				newRecord[i] = fmt.Sprintf("%f", fooDuration)
			case "BarDuration":
				barDuration = entryToFloatingPointSeconds(entry)
				newRecord[i] = fmt.Sprintf("%f", barDuration)
			case "TotalDuration":
				newRecord[i] = fmt.Sprintf("%f", fooDuration+barDuration)
			default:
				if transformer, ok := transformers[headers[i]]; ok {
					newRecord[i] = transformer(entry)
				} else {
					log.Fatalf("Don't know how to handle %s", headers[i])
				}
			}
		}

		w.Write(newRecord)
		w.Flush()

		if err := w.Error(); err != nil {
			log.Fatal(err)
		}
	}
}

func entryToFloatingPointSeconds(s string) float64 {
	var hours, minutes int
	var seconds float64

	s2 := strings.Replace(s, ":", " ", -1)
	_, err := fmt.Sscanf(s2, "%d %d %f", &hours, &minutes, &seconds)
	if err != nil {
		log.Fatalf("failed scanning: %s", err)
	}

	return float64((hours*3600)*(minutes*60)) + seconds
}

type TransformerFunc func(string) string

func rfc3339Pacific(s string) string {
	t, err := time.ParseInLocation(timestampFormat, s, mountainLoc)
	if err != nil {
		log.Fatal(err)
	}
	return t.In(pacificLoc).Format(time.RFC3339)
}

func passthrough(s string) string { return s }

func zeroPadLeft(s string) string {
	return fmt.Sprintf("%05s", s)
}

func fixUTF(s string) string {
	if utf8.ValidString(s) {
		return s
	} else {
		return strings.Map(func(r rune) rune {
			if r == utf8.RuneError {
				return -1
			}
			return r
		}, s)
	}
}

var transformers = map[string]TransformerFunc{
	"Timestamp": rfc3339Pacific,
	"Address":   passthrough,
	"ZIP":       zeroPadLeft,
	"FullName":  strings.ToUpper,
	"Notes":     fixUTF,
}
