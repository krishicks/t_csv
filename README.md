# t_csv

## Installation

```bash
go get gitlab.com/krishicks/t_csv
```

## Usage

```bash
cat some.csv | t_csv
```
